#!/bin/bash

# Launch the placeholder services
../../examples/cpp/agent/00_minimal/agent_00_minimal &
AGENT_PID=$!

../../examples/cpp/env/00_minimal/env_00_minimal &
ENV_PID=$!

# Launch the orchestrator services
PORT=9001 DATALOG_TYPE=raw DATALOG_FILE=./data.log ../orchestrator &
ORCH_PID=$!

# Make sure the orchestrator has had time to start
sleep 1s

# Start a session
TRIAL_ID=$(grpc_cli -json_output call 127.0.0.1:9001 cogment.Trial/Start "" | jq .trialId)

echo "trial id: $TRIAL_ID"
sleep 1s

# Cleanup after ourselves. This isn't great, but since this will always run in a container, meh.
kill $ORCH_PID
kill $ENV_PID
kill $AGENT_PID

sleep 1s

echo "smoke test completed successfully"
exit $SUCCESS