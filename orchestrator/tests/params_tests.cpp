
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "aom/orchestrator.h"
#include "aom/trial_spec.h"
#include "aom/trial_params.h"

using namespace std::chrono_literals;

using testing::_;
using testing::Return;
using testing::ByMove;


std::tuple<aom::Trial_spec, cogment::TrialParams> load_cogment_yaml(const std::string data) {
  auto raw = YAML::Load(data);

  auto spec = aom::Trial_spec(raw);
  auto params = aom::load_params(raw, spec);

  return {std::move(spec), std::move(params)};
}

// General purpose test fixture for the orchestrator
class Orchestrator_test : public ::testing::Test {
  easy_grpc::Environment grpc_env;
};

TEST_F(Orchestrator_test, no_params) {
  // The trial_params section is optional, as it could be created from scratch
  // by a hook.

  auto cogment_yaml = R"RAW(
import:
  proto:
    - tests.proto

actor_classes:
  - id: a
    action:
      space: aom.SingleValue
    observation:
      space: aom.SingleValue
  )RAW";

  auto [spec, params] = load_cogment_yaml(cogment_yaml);
  aom::Orchestrator orchestrator(std::move(spec), std::move(params), {});

  // But, trying to start a trial should obviously fail.
  EXPECT_ANY_THROW(orchestrator.start_trial({}).get());
}

class Mock_env {
  public:
    using service_type = cogment::Environment;

    ::cogment::EnvStartReply Start(::cogment::EnvStartRequest req, easy_grpc::Context ctx) {
      ++start_count;
      ::cogment::EnvStartReply rep;
      for(auto a : req.actor_counts()) {
        for(int i = 0 ; i < a; ++i) {
          rep.mutable_observation_set()->add_actors_map(0);
        }
      }


      auto obs = rep.mutable_observation_set()->add_observations();

      obs->set_snapshot(true);

      start_req = std::move(req);
      return rep;
    }

    ::cogment::EnvEndReply End(::cogment::EnvEndRequest, easy_grpc::Context ctx) {
      ++end_count;
      return {};
    }

    ::cogment::EnvUpdateReply Update(::cogment::EnvUpdateRequest, easy_grpc::Context ctx) {
      spdlog::info("update...");
      return {};
    }

    ::cogment::EnvOnMessageReply OnMessage(::cogment::EnvOnMessageRequest, easy_grpc::Context ctx) {
      spdlog::info("update...");
      return {};
    }


    ::cogment::VersionInfo Version(::cogment::VersionRequest, easy_grpc::Context ctx)  {
      return {};
    }


    ::cogment::EnvStartRequest start_req;
    int start_count = 0;
    int end_count = 0;
};

class Mock_agent {
  public:
    using service_type = cogment::Agent;

    ::cogment::AgentStartReply Start(::cogment::AgentStartRequest req, easy_grpc::Context ctx) {
      ++start_count;
      start_req = std::move(req);
      return {};
    }
    ::cogment::AgentEndReply End(::cogment::AgentEndRequest, easy_grpc::Context ctx) {
      ++end_count;
      return {};
    }
    ::cogment::AgentDecideReply Decide(::cogment::AgentDecideRequest, easy_grpc::Context ctx) {
      return {};
    }
    ::cogment::AgentRewardReply Reward(::cogment::AgentRewardRequest, easy_grpc::Context ctx) {
      return {};
    }
    ::cogment::AgentOnMessageReply OnMessage(::cogment::AgentOnMessageRequest, easy_grpc::Context ctx) {
      return {};
    }
    ::cogment::VersionInfo Version(::cogment::VersionRequest, easy_grpc::Context ctx) {
      return {};
    }


    ::cogment::AgentStartRequest start_req;
    int start_count = 0;
    int end_count = 0;
};

TEST_F(Orchestrator_test, no_actor) {
  Mock_env env;
  std::array<easy_grpc::Completion_queue, 1> server_queues;
  int env_server_port = 0;
  easy_grpc::server::Server env_server =
      easy_grpc::server::Config()
          .add_default_listening_queues(
              {server_queues.begin(), server_queues.end()})
          .add_service(env)
          .add_listening_port("127.0.0.1:0", {}, &env_server_port);

  auto cogment_yaml = fmt::format(R"RAW(
import:
  proto:
    - tests.proto

actor_classes:
  - id: a
    action:
      space: aom.SingleValue
    observation:
      space: aom.SingleValue

trial_params:
  environment:
    endpoint: grpc://127.0.0.1:{}
  )RAW", env_server_port);

  auto [spec, params] = load_cogment_yaml(cogment_yaml);
  aom::Orchestrator orchestrator(std::move(spec), std::move(params), {});

  orchestrator.start_trial({}).get();

  EXPECT_EQ(env.start_count, 1);
}


TEST_F(Orchestrator_test, no_config) {
  Mock_env env;
  Mock_agent agent;
  
  std::array<easy_grpc::Completion_queue, 1> server_queues;
  int env_server_port = 0;
  easy_grpc::server::Server env_server =
      easy_grpc::server::Config()
          .add_default_listening_queues(
              {server_queues.begin(), server_queues.end()})
          .add_service(env)
          .add_service(agent)
          .add_listening_port("127.0.0.1:0", {}, &env_server_port);

  auto cogment_yaml = fmt::format(R"RAW(
import:
  proto:
    - tests.proto

actor_classes:
  - id: a
    action:
      space: aom.SingleValue
    observation:
      space: aom.SingleValue

trial_params:
  environment:
    endpoint: grpc://127.0.0.1:{0}
  actors:
    - actor_class: a
      endpoint: grpc://127.0.0.1:{0}
    - actor_class: a
      endpoint: grpc://127.0.0.1:{0}
  )RAW", env_server_port);

  auto [spec, params] = load_cogment_yaml(cogment_yaml);
  aom::Orchestrator orchestrator(std::move(spec), std::move(params), {});

  orchestrator.start_trial({}).get();

  EXPECT_EQ(env.start_count, 1);
  EXPECT_EQ(agent.start_count, 2);
}



TEST_F(Orchestrator_test, send_trial_config_to_env_and_agents) {
  Mock_env env;
  Mock_agent agent;
  
  std::array<easy_grpc::Completion_queue, 1> server_queues;
  int env_server_port = 0;
  easy_grpc::server::Server env_server =
      easy_grpc::server::Config()
          .add_default_listening_queues(
              {server_queues.begin(), server_queues.end()})
          .add_service(env)
          .add_service(agent)
          .add_listening_port("127.0.0.1:0", {}, &env_server_port);

  auto cogment_yaml = fmt::format(R"RAW(
import:
  proto:
    - tests.proto

trial:
  config_type: aom.SingleValue

actor_classes:
  - id: a
    action:
      space: aom.SingleValue
    observation:
      space: aom.SingleValue

trial_params:
  trial_config:
    value: 12
  environment:
    endpoint: grpc://127.0.0.1:{0}
  actors:
    - actor_class: a
      endpoint: grpc://127.0.0.1:{0}
  )RAW", env_server_port);

  auto [spec, params] = load_cogment_yaml(cogment_yaml);
  aom::Orchestrator orchestrator(std::move(spec), std::move(params), {});

  orchestrator.start_trial({}).get();


  EXPECT_TRUE(env.start_req.trial_config().content() != "");
  EXPECT_TRUE(agent.start_req.trial_config().content() != "");
}