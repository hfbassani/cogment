#include "orchestrator_fixture.h"

using namespace std::chrono_literals;

using testing::_;
using testing::Return;
using testing::ByMove;

TEST_F(Orchestrator_test, action_success) {
  auto trial = start_trial();

  cogment::TrialActionRequest req;
  req.set_trial_id(trial.trial_id());


  cogment::EnvUpdateReply env_update_rep;
  //One single delta observation that goes to both actors...
  auto observation = env_update_rep.mutable_observation_set()->add_observations();
  observation->set_snapshot(false);
  observation->set_content("");

  env_update_rep.mutable_observation_set()->add_actors_map(0);
  env_update_rep.mutable_observation_set()->add_actors_map(0);

  for (int i = 0; i < 100; ++i) {
    EXPECT_CALL(env, Update(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future(env_update_rep))));

    if( i != 0) {
      EXPECT_CALL(*agent, Reward(_,_))
        .Times(1)
        .WillOnce(Return(ByMove(ready_future<cogment::AgentRewardReply>())));
    }
    if( i != 0) {
      EXPECT_CALL(*agent, OnMessage(_,_))
        .Times(1)
        .WillOnce(Return(ByMove(ready_future<cogment::AgentOnMessageReply>())));
    }
    EXPECT_CALL(*agent, Decide(_,_))
      .Times(1)
      .WillOnce(Return(ByMove(ready_future<cogment::AgentDecideReply>())));

    auto rep = orchestrator->Action(req);
    rep.get();   
  }

  end_trial(trial);
}
