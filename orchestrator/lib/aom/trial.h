#ifndef AOM_ORCHESTRATOR_TRIAL_H
#define AOM_ORCHESTRATOR_TRIAL_H

#include "easy_grpc/easy_grpc.h"

#include "cogment/api/agent.pb.h"
#include "cogment/api/data.pb.h"
#include "cogment/api/environment.egrpc.pb.h"
#include "cogment/api/environment.pb.h"
#include "cogment/api/orchestrator.pb.h"

#include "aom/actor.h"
#include "aom/datalog/storage_interface.h"
#include "aom/stub_pool.h"

#include "uuid.h"

#include <chrono>
#include <deque>
#include <memory>
#include <optional>

namespace aom {
class Trial_action_handler;
class Orchestrator;

enum class Trial_state {
  Initializing,
  Ready,
  Busy,
  Terminating,
};

class Trial : public std::enable_shared_from_this<Trial> {
  static uuids::uuid_system_generator id_generator_;

 public:
  Trial(Orchestrator* owner, std::string user_id);
  ~Trial();
  Trial(Trial&&) = default;
  Trial& operator=(Trial&&) = default;

  // The id of the trial itself
  const uuids::uuid& id() { return raw_id_; }

  // The actor id of the human actor.
  int human_actor_id() const;

  ::easy_grpc::Future<void> configure(cogment::TrialParams);

  ::easy_grpc::Future<::cogment::TrialActionReply> user_acted(
      cogment::TrialActionRequest req);

  void terminate();

  Trial_state state() const { return state_; }

  void mark_busy() { state_ = Trial_state::Busy; }

  void consume_feedback(
      const ::google::protobuf::RepeatedPtrField<::cogment::Feedback>&);

  void consume_message(
      const ::google::protobuf::RepeatedPtrField<::cogment::Message>&);

  void populate_observation(int actor_id, ::cogment::Observation* obs);

  void heartbeat();

  bool is_stale();

  const std::vector<int>& actor_counts() const { return actor_counts_; }

  const cogment::TrialParams& params() const { return params_; }

 private:
  Trial(const Trial&) = delete;
  Trial& operator=(const Trial&) = delete;

  void begin();
  void prepare_actions();
  void gather_actions();
  void send_final_observation();
  void dispatch_update();
  void refresh_activity();
  void strip_observation(cogment::ObservationSet& observation);

  cogment::TrialParams params_;

  std::chrono::time_point<std::chrono::steady_clock> last_activity_;

  const uuids::uuid raw_id_;
  const std::string id_;
  const std::string user_id_;

  Orchestrator* const owner_;
  std::vector<std::unique_ptr<Actor>> actors_;
  std::vector<int> actor_counts_;
  std::atomic<Trial_state> state_ = Trial_state::Initializing;

  cogment::ObservationSet latest_observation_;
  std::atomic<int> pending_decisions_;
  std::vector<expected<cogment::Action>> actions_;

  std::shared_ptr<aom::Stub_pool<cogment::Environment>::Entry> env_stub_;

  std::unique_ptr<Trial_log_interface> log_interface_;
  std::deque<cogment::DatalogSample> step_data_;

  std::vector<grpc_metadata> headers_;
  easy_grpc::client::Call_options call_options_;
};
}  // namespace aom
#endif