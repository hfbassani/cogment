#include "string.h"
#include "time.h"

#include "aom/trial.h"

#include "aom/agent.h"
#include "aom/human.h"
#include "aom/orchestrator.h"

#include "spdlog/spdlog.h"

namespace {

// TODO: Move to a utility library (and decide how to handle error)
//       i.e. exceptions or error returns
uint64_t get_timestamp() {
  static constexpr uint64_t NANOS_PER_SEC = 1'000'000'000;

  struct timespec ts;
  if (clock_gettime(CLOCK_REALTIME, &ts) == 0) {
    return ((ts.tv_sec * NANOS_PER_SEC) + ts.tv_nsec);
  } else {
    spdlog::error("clock_gettime failed: {}", strerror(errno));
    return 0;
  }
}

void recompute_reward(cogment::Reward* reward) {
  float value = 0.0f;

  auto feedback_count = reward->feedbacks_size();
  if (feedback_count > 0) {
    float total_c = 0.0f;
    for (int i = 0; i < feedback_count; ++i) {
      auto v = reward->feedbacks(i).value();
      auto c = reward->feedbacks(i).confidence();

      value += v * c;
      total_c += c;
    }

    value /= total_c;
  }
  reward->set_value(value);
  reward->set_confidence(1.0f);
}

auto* update_reward(cogment::DatalogSample* sample,
                    const cogment::Feedback& feedback) {
  auto* const reward = sample->mutable_rewards(feedback.actor_id());
  auto* const new_fb = reward->add_feedbacks();
  new_fb->CopyFrom(feedback);
  new_fb->set_tick_id(sample->observations().tick_id());
  recompute_reward(reward);

  return reward;
}

}  // namespace

namespace aom {
uuids::uuid_system_generator Trial::id_generator_;

Trial::Trial(Orchestrator* owner, std::string user_id)
    : raw_id_(id_generator_()),
      id_(to_string(raw_id_)),
      user_id_(std::move(user_id)),
      owner_(owner) {
  spdlog::info("creating trial: {}", id_);
  log_interface_ = owner->get_storage()->begin_trial(this);
  refresh_activity();
}

Trial::~Trial() {
  for (auto& sample : step_data_) {
    log_interface_->add_sample(std::move(sample));
  }
  step_data_.clear();

  for (auto& metadata : headers_) {
    grpc_slice_unref(metadata.key);
    grpc_slice_unref(metadata.value);
  }
}

::easy_grpc::Future<void> Trial::configure(cogment::TrialParams cfg) {
  params_ = std::move(cfg);

  grpc_metadata trial_header;
  trial_header.key = grpc_slice_from_static_string("trial_id");
  trial_header.value = grpc_slice_from_copied_string(id_.c_str());
  headers_.push_back(trial_header);

  call_options_.headers = &headers_;

  // Launch the environment
  env_stub_ = owner_->env_stubs_.get_stub(params_.environment().endpoint());

  // Launch the environment, in parallell with the agent launches
  std::vector<aom::Future<void>> agents_ready;
  ::cogment::EnvStartRequest env_start_req;

  env_start_req.set_trial_id(id_);

  if (params_.environment().has_config()) {
    *env_start_req.mutable_config() = params_.environment().config();
  }

  if (params_.has_trial_config()) {
    *env_start_req.mutable_trial_config() = params_.trial_config();
  }

  // List of actors, organized per actor class
  std::vector<std::vector<std::unique_ptr<Actor>>> actors_per_class(
      owner_->trial_spec_.actor_classes.size());
  actor_counts_.resize(owner_->trial_spec_.actor_classes.size(), 0);

  // Launch the actors, both AI and human
  for (const auto& actor_info : params_.actors()) {
    auto class_id = owner_->trial_spec_.get_class_id(actor_info.actor_class());
    auto url = actor_info.endpoint();
    actor_counts_[class_id] += 1;

    if (url == "human") {
      auto human_actor = std::make_unique<Human>(id_);
      human_actor->actor_class = &owner_->trial_spec_.actor_classes[class_id];
      actors_per_class.at(class_id).push_back(std::move(human_actor));
    } else {
      std::optional<std::string> config;
      if (actor_info.has_config()) {
        config = actor_info.config().content();
      }
      auto stub_entry = owner_->agent_stubs_.get_stub(url);
      auto actor = std::make_unique<Agent>(this, stub_entry, config);
      actor->actor_class = &owner_->trial_spec_.actor_classes[class_id];
      actors_per_class.at(class_id).push_back(std::move(actor));
    }
  }

  // Assign actor ids, and perform initialization
  int aid = 0;
  for (auto& a_class : actors_per_class) {
    for (auto& actor : a_class) {
      actor->set_actor_id(aid++);
      agents_ready.push_back(actor->init());

      actors_.push_back(std::move(actor));
    }
  }

  for (auto i : actor_counts_) {
    env_start_req.add_actor_counts(i);
  }

  auto env_ready = (*env_stub_)->Start(std::move(env_start_req), call_options_);

  return join(env_ready, concat(agents_ready.begin(), agents_ready.end()))
      .then([this](cogment::EnvStartReply env_rep) {
        // Everyone is ready,
        latest_observation_ = std::move(*env_rep.mutable_observation_set());
        begin();
      });
}

void Trial::begin() { gather_actions(); }

void Trial::terminate() {
  state_ = Trial_state::Terminating;

  auto self_lock = shared_from_this();

  cogment::EnvEndRequest env_req;
  env_req.set_trial_id(to_string(id()));
  (*env_stub_)->End(env_req, call_options_).finally([self_lock](auto) {});

  for (auto& actor : actors_) {
    actor->terminate();
  }
}

int Trial::human_actor_id() const {
  int result = 0;
  for (auto& actor : actors_) {
    if (actor->is_human()) {
      return result;
    }
    ++result;
  }
  return -1;
}

void Trial::populate_observation(int actor_id, ::cogment::Observation* obs) {
  auto obs_index = latest_observation_.actors_map(actor_id);
  *obs->mutable_data() = latest_observation_.observations(obs_index);

  obs->set_tick_id(latest_observation_.tick_id());
  obs->set_timestamp(latest_observation_.timestamp());
}

// Removes from an observation set all the fields that were requested
// to be cleared from it.
void Trial::strip_observation(cogment::ObservationSet& observation_set) {
  std::set<int> processed;

  for (std::size_t i = 0; i < actors_.size(); ++i) {
    int index = observation_set.actors_map(i);
    if (processed.count(index)) {
      continue;
    }
    processed.insert(index);

    auto actor_class = actors_[i]->actor_class;

    auto& data = *observation_set.mutable_observations(index);
    if (data.snapshot()) {
      if (actor_class->cleared_observation_fields.size() > 0) {
        assert(actors_[i]->actor_class->observation_space_prototype);
        auto msg = actors_[i]->actor_class->observation_space_prototype->New();
        msg->ParseFromString(data.content());
        auto refl = msg->GetReflection();
        for (auto f : actor_class->cleared_observation_fields) {
          refl->ClearField(msg, f);
        }
        msg->SerializeToString(data.mutable_content());
      }
    } else {
      if (actor_class->cleared_delta_fields.size() > 0) {
        assert(actors_[i]->actor_class->observation_delta_prototype);
        auto msg = actors_[i]->actor_class->observation_delta_prototype->New();
        msg->ParseFromString(data.content());
        auto refl = msg->GetReflection();
        for (auto f : actor_class->cleared_delta_fields) {
          refl->ClearField(msg, f);
        }
        msg->SerializeToString(data.mutable_content());
      }
    }
  }
}

void Trial::prepare_actions() {
  static constexpr uint64_t MIN_NB_BUFFERED_SAMPLES =
      2;  // Because of the way we use the buffer
  static constexpr uint64_t NB_BUFFERED_SAMPLES =
      5;                                         // Could be an external setting
  static constexpr uint64_t LOG_BATCH_SIZE = 1;  // Could be an external setting
  static constexpr uint64_t LOG_TRIGGER_SIZE =
      NB_BUFFERED_SAMPLES + LOG_BATCH_SIZE - 1;
  static_assert(NB_BUFFERED_SAMPLES >= MIN_NB_BUFFERED_SAMPLES);
  static_assert(LOG_BATCH_SIZE > 0);

  // Send overflow to log
  if (step_data_.size() >= LOG_TRIGGER_SIZE) {
    while (step_data_.size() >= NB_BUFFERED_SAMPLES) {
      log_interface_->add_sample(std::move(step_data_.front()));
      step_data_.pop_front();
    }
  }

  const bool first_sample = step_data_.empty();
  step_data_.emplace_back();
  auto& sample = step_data_.back();

  sample.set_timestamp(get_timestamp());

  sample.mutable_observations()->CopyFrom(latest_observation_);
  strip_observation(*sample.mutable_observations());
  if (sample.observations().tick_id() == 0 && !first_sample) {
    const auto prev_tick =
        step_data_[step_data_.size() - 2].observations().tick_id();
    sample.mutable_observations()->set_tick_id(prev_tick + 1);
  }

  sample.set_trial_id(id_);
  sample.set_user_id(user_id_);

  const auto nb_actors = actors_.size();
  sample.mutable_actions()->Reserve(nb_actors);
  sample.mutable_rewards()->Reserve(nb_actors);
  sample.mutable_messages()->Reserve(nb_actors);
  for (unsigned int i = 0; i < nb_actors; ++i) {
    step_data_.back().mutable_actions()->Add();
    step_data_.back().mutable_rewards()->Add();
    step_data_.back().mutable_messages()->Add();
  }

  state_ = Trial_state::Ready;

  if (latest_observation_.actors_map_size() != static_cast<int>(nb_actors)) {
    spdlog::error(
        "Trial {}, env generated observations for {} actors, but expected {}",
        id_, latest_observation_.actors_map_size(), nb_actors);
    throw std::runtime_error("actor count mismatch");
  }

  pending_decisions_ = nb_actors;
  actions_.resize(nb_actors);
}

void Trial::gather_actions() {
  prepare_actions();
  auto self = shared_from_this();

  for (int i = 0; i < latest_observation_.actors_map_size(); ++i) {
    cogment::Observation observation;
    populate_observation(i, &observation);

    auto actor_decision_fut =
        actors_[i]->request_decision(std::move(observation));
    actor_decision_fut.finally([self, this, i](auto rep) {
      if (rep.has_value()) {
        step_data_.back().mutable_actions(i)->CopyFrom(*rep);
      }

      actions_[i] = std::move(rep);

      if (--pending_decisions_ == 0) {
        // Send the actions
        dispatch_update();
      }
    });
  }
}

void Trial::send_final_observation() {
  prepare_actions();
  auto self = shared_from_this();

  for (int i = 0; i < latest_observation_.actors_map_size(); ++i) {
    cogment::Observation observation;

    populate_observation(i, &observation);
    actors_[i]->send_final_observation(std::move(observation));
  }
}

void Trial::consume_feedback(
    const ::google::protobuf::RepeatedPtrField<::cogment::Feedback>&
        feedbacks) {
  refresh_activity();

  for (const auto& feedback : feedbacks) {
    const auto time = feedback.tick_id();

    if (time < 0) {
      // Feedback for current tick
      auto& sample = step_data_.back();
      update_reward(&sample, feedback);

    } else {
      // Feedback for older tick
      const uint64_t tick = time;

      if (tick >= step_data_.front().observations().tick_id()) {
        // We still have the sample: update it
        auto res = std::find_if(
            step_data_.begin(), step_data_.end(), [tick](const auto& item) {
              return (item.observations().tick_id() == tick);
            });
        if (res == step_data_.end()) {
          spdlog::error("Invalid tick for feedback: {} > {}", tick,
                        step_data_.back().observations().tick_id());
          continue;
        }
        update_reward(&(*res), feedback);

      } else {
        // Sample has already been logged: log a feedback-only sample

        cogment::DatalogSample sample;
        sample.set_timestamp(get_timestamp());
        sample.set_trial_id(id_);
        sample.set_user_id(user_id_);
        sample.mutable_observations()->set_tick_id(tick);
        sample.mutable_rewards()->Reserve(actors_.size());
        for (unsigned int i = 0; i < actors_.size(); ++i) {
          sample.mutable_rewards()->Add();
        }

        auto reward = update_reward(&sample, feedback);

        // TODO: Do we want to do this now (it was "missing" before)
        actors_[feedback.actor_id()]->dispatch_reward(tick, *reward);

        log_interface_->add_sample(std::move(sample));
      }
    }
  }
}

void Trial::consume_message(
    const ::google::protobuf::RepeatedPtrField<::cogment::Message>& messages) {
  refresh_activity();

  for (const auto& message : messages) {
    if (message.receiver_id() == -1) {
      cogment::EnvOnMessageRequest env_onmessage_req;
      env_onmessage_req.set_actor_id(message.receiver_id());
      env_onmessage_req.set_trial_id(id_);
      env_onmessage_req.add_messages()->CopyFrom(message);

      (*env_stub_)->OnMessage(std::move(env_onmessage_req)).finally([](auto) {
      });
    } else {
      auto* onmessage =
          step_data_.back().mutable_messages(message.receiver_id());
      auto new_msg = onmessage->add_messages();
      new_msg->CopyFrom(message);
    }
  }
}

void Trial::dispatch_update() {
  if (state_ == Trial_state::Terminating) return;

  auto self = shared_from_this();

  if (step_data_.size() > 1) {
    const auto& last_sample = step_data_[step_data_.size() - 2];

    // Send the previously acquired rewards
    for (int index = 0; index < last_sample.rewards_size(); ++index) {
      actors_[index]->dispatch_reward(last_sample.observations().tick_id(),
                                      last_sample.rewards(index));
    }

    // Send the previously acquired messages
    for (int index = 0; index < last_sample.messages_size(); ++index) {
      actors_[index]->dispatch_onmessage(last_sample.messages(index));
    }
  }

  cogment::EnvUpdateRequest req;
  req.set_trial_id(id_);
  req.set_reply_with_snapshot(false);
  for (const auto& act : actions_) {
    if (act.has_value()) {
      req.mutable_action_set()->add_actions(act->content());
    } else {
      req.mutable_action_set()->add_actions("");
    }
  }

  auto env_update_reply = (*env_stub_)->Update(std::move(req), call_options_);
  env_update_reply.finally([self, this](auto rep) {
    try {
      if (rep.has_value()) {
        latest_observation_ = std::move(*rep->mutable_observation_set());
        consume_feedback(rep->feedbacks());
        consume_message(rep->messages());

        if ((params_.max_steps() != 0 &&
             step_data_.back().observations().tick_id() - 1 >=
                 params_.max_steps())) {
          owner_->end_trial(id());

        } else {
          const bool is_going_to_end = rep->end_trial();
          if (is_going_to_end) {
            send_final_observation();

            owner_->end_trial(id());
          } else {
            gather_actions();
          }
        }
      } else {
        // TODO: Handle this
        spdlog::error("Unhandled condition!");
      }
    } catch (const std::exception& e) {
      spdlog::error(e.what());
    }
  });
}

::easy_grpc::Future<::cogment::TrialActionReply> Trial::user_acted(
    cogment::TrialActionRequest user_req) {
  refresh_activity();

  int aid = user_req.actor_id();

  auto self = this->shared_from_this();
  auto& actor = actors_.at(aid);

  step_data_.back()
      .mutable_actions(human_actor_id())
      ->CopyFrom(user_req.action());

  return actor->user_acted(std::move(user_req));
}

void Trial::heartbeat() { refresh_activity(); }

void Trial::refresh_activity() {
  last_activity_ = std::chrono::steady_clock::now();
}

bool Trial::is_stale() {
  auto dt = std::chrono::duration_cast<std::chrono::seconds>(
      std::chrono::steady_clock::now() - last_activity_);
  bool stale = std::chrono::steady_clock::now() - last_activity_ >
               std::chrono::seconds(params_.max_inactivity());
  spdlog::info("stale check: {} vs {}, {}", params_.max_inactivity(),
               dt.count(), stale);
  return params_.max_inactivity() > 0 && stale;
}

}  // namespace aom
