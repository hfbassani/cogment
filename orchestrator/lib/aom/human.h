#ifndef AOM_ORCHESTRATOR_HUMAN_H
#define AOM_ORCHESTRATOR_HUMAN_H

#include "aom/actor.h"

namespace aom {

class Human : public Actor {
public:
  Human(std::string tid);
  ~Human();

  Future<void> init() override;
  void terminate() override;
  void send_final_observation(cogment::Observation&& obs) override;

  bool is_human() const override {return true;}
  
  void dispatch_reward(int tick_id, const ::cogment::Reward& reward) override {
    latest_reward_ = reward;
  }

  void dispatch_onmessage(const ::cogment::MessageCollection& messages) override {
    latest_messages_ = messages;
  }


  Future<cogment::Action> request_decision(cogment::Observation&& obs) override;
  ::easy_grpc::Future<::cogment::TrialActionReply> user_acted(
    cogment::TrialActionRequest req
  ) override ;

private:
  std::optional<::cogment::Reward> latest_reward_;
  std::optional<::cogment::MessageCollection> latest_messages_;
  Promise<cogment::Action> human_action_promise_;
  Promise<::cogment::TrialActionReply> human_observation_promise_;
};
}  // namespace aom
#endif