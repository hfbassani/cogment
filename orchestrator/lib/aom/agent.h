#ifndef AOM_ORCHESTRATOR_AGENT_H
#define AOM_ORCHESTRATOR_AGENT_H

#include "aom/actor.h"
#include "aom/stub_pool.h"
#include "cogment/api/agent.egrpc.pb.h"

#include <optional>

namespace aom {

class Trial;
class Agent : public Actor {
public:
  using stub_type = std::shared_ptr<aom::Stub_pool<cogment::Agent>::Entry>;
  Agent(Trial* owner, stub_type stub, std::optional<std::string> config_data);
  ~Agent();
  
  Future<void> init() override;
  void terminate() override;

  void dispatch_reward(int tick_id, const ::cogment::Reward& reward) override;
  void dispatch_onmessage(const ::cogment::MessageCollection& messages) override;
  Future<cogment::Action> request_decision(cogment::Observation&& obs) override;

  ::easy_grpc::Future<::cogment::TrialActionReply> user_acted(
    cogment::TrialActionRequest req
  ) override { 
    throw std::runtime_error("agent is recieving human action.");
   }

private:
  Trial* owner_;
  stub_type stub_;
  cogment::Action latest_action_;
  std::optional<std::string> config_data_;

};
}  // namespace aom
#endif