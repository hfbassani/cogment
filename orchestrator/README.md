# aom-orchestrator

# Hacking on the orchestrator

```
# Launch a dev_env docker container with the framework source mounted
me@machine:~/git/framework/orchestrator$ ./dev_env.sh

# Build the orchestrator
machine:/app$ mkdir _bld
machine:/app$ cd _bld
machine:/app$ cmake ..
machine:/app$ make -j $(nproc) orchestrator

# Run tests
machine:/app$ ctest

# Launch the orchestrator
machine:/app$ ./orchestrator/orchestrator
```
