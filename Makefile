SHELL:=/bin/sh
ifeq ($(REGISTRY_IMAGE),)
    REGISTRY_IMAGE:=registry.gitlab.com/cogment/cogment
endif

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_path := $(patsubst %/,%,$(dir $(mkfile_path)))

all: protos

api_messages = api/cogment/api/common.proto
api_services = api/cogment/api/data.proto api/cogment/api/agent.proto api/cogment/api/environment.proto api/cogment/api/orchestrator.proto api/cogment/api/hooks.proto

protos: messages services

#messages: $(api_messages) cogment-builder
messages: $(api_messages)

	docker run -it --rm -v $(current_path):/app ${REGISTRY_IMAGE}:builder protoc -I./api \
	--plugin="protoc-gen-ts=/usr/local/bin/protoc-gen-ts" \
	--ts_out=service=grpc-web:./sdk_js \
	--python_out=./sdk_python \
	--js_out=import_style=commonjs,binary:./sdk_js \
	$(api_messages)

#services: $(api_services) cogment-builder
services: $(api_services)
	docker run -it --rm -v $(current_path):/app ${REGISTRY_IMAGE}:builder protoc -I./api \
		--plugin="protoc-gen-ts=/usr/local/bin/protoc-gen-ts" \
		--ts_out=service=grpc-web:./sdk_js \
		--js_out=import_style=commonjs,binary:./sdk_js \
		--grpc_web_out=import_style=commonjs,mode=grpcweb:./sdk_js \
		--grpc_python_out=./sdk_python \
		--python_out=./sdk_python \
		$(api_services)

cogment-builder: Dockerfile.builder
	docker build -f $< --cache-from ${REGISTRY_IMAGE}:builder -t ${REGISTRY_IMAGE}:builder .

cogment-cpp-builder: Dockerfile.cpp_builder cogment-builder
	docker build -f $< --cache-from ${REGISTRY_IMAGE}:cpp_builder -t ${REGISTRY_IMAGE}:cpp_builder .

# This builds the production-ready docker image for the orchestrator
cogment-orchestrator: Dockerfile.orchestrator
	docker build -f $< -t ${REGISTRY_IMAGE}/orchestrator .

cpp_files = orchestrator/main.cpp \
	orchestrator/lib/aom/datalog/storage_interface.cpp \
	orchestrator/lib/aom/datalog/storage_interface.h \
	orchestrator/lib/aom/orchestrator.cpp \
	orchestrator/lib/aom/orchestrator.h \
	orchestrator/lib/aom/trial.h \
	orchestrator/lib/aom/trial.cpp \
	orchestrator/lib/aom/trial_params.h \
	orchestrator/lib/aom/trial_params.cpp \
	orchestrator/lib/aom/trial_spec.h \
	orchestrator/lib/aom/trial_spec.cpp

	
cpp_format: $(cpp_files)
	docker run --rm --user $(shell id -u ${USER}):$(shell id -g ${USER}) -v ${PWD}:/app ${REGISTRY_IMAGE}:builder clang-format -i -style=file $^

docs_serve:
	docker run --rm -it -p 8000:8000 -v ${PWD}:/docs squidfunk/mkdocs-material

docs_grpc:
	docker run --rm  \
	-v ${PWD}/docs/sdk:/out \
	-v ${PWD}/api:/protos \
	pseudomuto/protoc-gen-doc \
	--doc_opt=markdown,grpc.md \
	protos/cogment/api/agent.proto \
	protos/cogment/api/common.proto \
	protos/cogment/api/data.proto \
	protos/cogment/api/environment.proto \
	protos/cogment/api/orchestrator.proto 

# docs_js_sdk:
# 	docker run --rm \
# 	-v ${PWD}/sdk_js:/src \
# 	jsdoc2md:latest \
# 	aom_framework/*.js \
# 	> ./docs/sdk/javascript.md

# docs_python:
# 	docker run --rm  \
# 	-v ${PWD}/sdk_python:/app \
# 	pydoc2md:latest \
# 	simple aom_framework+ \
# 	> ./docs/sdk/python.md

