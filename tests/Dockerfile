# This Dockerfile expects the following:
# 1 - The cogment repository's root be used as the build context
# 2 - That a local docker image tagged cogment_orchestrator:local be present
#     and contains the orchestrator matching the current state of the repo

# Building the orchestrator can be long and complicated, leverage the existing image
FROM cogment_orchestrator:local as orchestrator
FROM ubuntu:19.10

# Get an orchestrator from whatever the current locally built one is
COPY --from=orchestrator /usr/local/bin/orchestrator /usr/local/bin/
RUN apt-get update && \
    apt-get install -y wget protobuf-compiler make python3 python3-pip && \
    wget https://dl.google.com/go/go1.13.3.linux-amd64.tar.gz && \
    tar -xvf go1.13.3.linux-amd64.tar.gz && \
    chmod +x go && \
    mv go /usr/local 

ENV GOROOT=/usr/local/go
ENV PATH=$GOPATH/bin:$GOROOT/bin:$PATH
ADD cli /cogment_cli
RUN cd /cogment_cli && make build-linux && mv build/cogment-linux-amd64 /usr/local/bin/cogment

# We'l be using the python SDK to run the tests
ADD sdk_python /cogment_py
RUN pip3 install -e /cogment_py

ADD tests /cogment_tests
WORKDIR /cogment_tests


RUN cd simple && cogment generate --python_dir=.
RUN cd multi_proto && cogment generate --python_dir=.

RUN pip3 install pytest
ENV PYTHONPATH=/cogment_tests

CMD ["pytest"]