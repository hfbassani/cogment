import os
import sys
test_dir = os.path.dirname(__file__)
sys.path.append(test_dir)


import cog_mocks
import cogment
from cogment.client import Connection
import cog_settings
import threading
import subprocess
import time
import pytest
import tempfile


@pytest.fixture
def setup():
    # Launch the services
    services = [
        cog_mocks.get_mock_agent_class(
            cog_settings, cog_settings.actor_classes.agent),
        cog_mocks.get_mock_env_class(cog_settings)
    ]

    server = cogment.GrpcServer(services, cog_settings, port=9001)
    server_thread = threading.Thread(target=server.serve)
    server_thread.start()

    # Prepare the orchestrator status sync file
    fifo_dir = tempfile.mkdtemp()
    fifo_file = os.path.join(fifo_dir, 'orch_status')
    os.mkfifo(fifo_file)

    # Launch the orchestrator
    orch_proc = subprocess.Popen(["orchestrator",
                                  "--port=9000",
                                  f"--status_file={fifo_file}"
                                  ], cwd=test_dir)

    # Wait for the orchestrator to be ready
    orch_status = open(fifo_file, 'r')
    expected_i = orch_status.read(1)
    assert(expected_i == "I")

    expected_r = orch_status.read(1)
    assert(expected_r == "R")

    # Execute the test
    yield None

    # Kill the orchestrator
    orch_proc.terminate()
    orch_proc.wait()

    expected_t = orch_status.read(1)
    assert(expected_t == "T")

    # Get rid of the sync file
    orch_status.close()
    os.remove(fifo_file)
    os.rmdir(fifo_dir)

    # Kill the service
    server.stop()
    server_thread.join()


@pytest.mark.parametrize("streaming", [True, False])
def test_simple_trial(setup, streaming):
    # launch a client
    conn = Connection(cog_settings, "localhost:9000")

    trial = conn.start_trial(cog_settings.actor_classes.agent)

    if streaming:
        trial.begin_actions_stream()

    action_space = cog_settings.actor_classes.agent.action_space

    trial.do_action(action_space())
    trial.do_action(action_space())
    trial.do_action(action_space())

    trial.end()


@pytest.mark.parametrize("streaming", [True, False])
def test_trial_no_action(setup, streaming):
    # launch a client
    conn = Connection(cog_settings, "localhost:9000")

    trial = conn.start_trial(cog_settings.actor_classes.agent)
    if streaming:
        trial.begin_actions_stream()

    trial.end()
