import cogment


def get_mock_agent_class(settings, a_class):
    """Creates a blank agent implementation"""
    class MockAgent(cogment.Agent):
        VERSIONS = {"mock_agent": "1.0.0"}
        actor_class = a_class

        def decide(self, observation):
            return a_class.action_space()

        def reward(self, reward):
            pass

        def end(self):
            pass

    return MockAgent


def get_mock_env_class(settings):
    """Creates a blank environment implementation"""
    class MockEnv(cogment.Environment):
        VERSIONS = {"mock_env": "1.0.0"}

        def get_observation_table(self):
            result = settings.ObservationsTable(self.trial)

            for ac in settings.actor_classes:
                # Create an observation of that classes' observation space
                observation = ac.observation_space()

                # Assign it to all actors of that class
                for o in getattr(result, ac.id):
                    o.snapshot = observation

            return result

        def start(self, config):
            return self.get_observation_table()

        def update(self, actions):
            return self.get_observation_table()

    return MockEnv
