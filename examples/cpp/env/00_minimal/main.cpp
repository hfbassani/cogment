#include "cogment/api/environment.egrpc.pb.h"

#include <grpcpp/grpcpp.h>
#include <spdlog/spdlog.h>

#include <sys/time.h>

#include "easy_grpc/easy_grpc.h"
#include "easy_grpc_reflection/reflection.h"

#include <iostream>

namespace rpc = easy_grpc;

class Env_service {
public:
    int count = 0;
    using service_type = cogment::Environment;
    
    // Called when a new trial is created.

    cogment::EnvStartReply Start(::cogment::EnvStartRequest, easy_grpc::Context) {
      cogment::EnvStartReply reply;
      
      
      auto observation = reply.mutable_observation_set()->add_observations();
      observation->set_snapshot(true);
      observation->set_content("");

      reply.mutable_observation_set()->add_actors_map(0);
      reply.mutable_observation_set()->add_actors_map(0);

      std::cout << "env_start\n";
      return reply;
    }

    cogment::EnvUpdateReply Update(::cogment::EnvUpdateRequest, easy_grpc::Context) {
      cogment::EnvUpdateReply reply;

      struct timespec ts;
      if (clock_gettime(CLOCK_REALTIME, &ts) != 0) {
        throw std::runtime_error("Error from 'clock_gettime'");
      }
      static constexpr uint64_t NANO_PER_SEC = 1'000'000'000;
      uint64_t timestamp = (ts.tv_sec * NANO_PER_SEC) + ts.tv_nsec;

      reply.mutable_observation_set()->set_tick_id(count);
      reply.mutable_observation_set()->set_timestamp(timestamp);

      auto observation = reply.mutable_observation_set()->add_observations();
      observation->set_snapshot(false);
      observation->set_content("");
      reply.mutable_observation_set()->add_actors_map(0);
      reply.mutable_observation_set()->add_actors_map(0);

      std::cout << "env_update:" << count++ << "\n";
      return reply;
    }

    cogment::EnvEndReply End(::cogment::EnvEndRequest, easy_grpc::Context) {
      return {};
    }

    cogment::EnvOnMessageReply OnMessage(::cogment::EnvOnMessageRequest, easy_grpc::Context) {
      return {};
    }


    cogment::VersionInfo Version(::cogment::VersionRequest, easy_grpc::Context) {
      return {};
    }
};

int main(int, const char** ) {
  rpc::Environment grpc_env;
  std::vector<rpc::Completion_queue> server_cqs(4);


  Env_service service;

  rpc::server::Server server( rpc::server::Config()
    // 
    .add_default_listening_queues({server_cqs.begin(), server_cqs.end()})
    // Use our service
    .add_service(service)

    // Open an unsecured port
    .add_listening_port("0.0.0.0:9003")

    // enable reflection
    .add_feature(easy_grpc::Reflection_feature()));

    std::cout << "env service running...\n";
  // Arrrrrrrrrrrg
  while(1) {
    std::this_thread::sleep_for(std::chrono::minutes(2));
  }

  return 0;
}