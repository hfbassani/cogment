# Contributing

## Reporting bugs / request improvements

If you see something wrong, or there is a change you would like to see. Simply file an [issue](https://gitlab.com/cogment/cogment/issues) for it.

If you are unsure whether something is a bug or not, or if you are just looking
for information on how to use the framework, drop in to our [slack channel](https://cogment.slack.com) 
and ask us before filling an issue.

## Pull requests.

Pull requests must match an [issue](https://gitlab.com/cogment/cogment/issues).
Pull requests will only be reviewed once all CI tests are green on it.

## Testing

Ideally, every commit should be accompanied with matching tests within reason. Tests that 
are effectivaly a reimplementation of the tested code should be simply ommited.

## Coding style

The cogment project contains code in a variety of programming languages, and as such,
there is no monolithic code formatting policy in place. That being said, coding standards
are enforced by the CI whenever possible.

That being said, code that is not checked by the CI is still expected to be stylistically consistent
with what's around it.

