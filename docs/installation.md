# How to Download and Install Cogment

## Pre-Requisites

Please install:

1. [Docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/)

2. [protoc](https://github.com/protocolbuffers/protobuf)

## Get the Cogment CLI

The latest `cogment` CLI is available [here](https://gitlab.com/cogment/cogment/-/releases) as an executable binary.

Choose the appropriate file for your system, rename the file to "cogment", and make sure to put it in a folder that is in your 'PATH' environmental variable.

You can then list all the commands by typing:

```text
$ cogment help
```

or for help on each individual command:

```text
$ cogment help <command>
```

You are now ready to start using Cogment!
We recommend our [cogment tutorial](tutorial/intro.md) as the next step.
