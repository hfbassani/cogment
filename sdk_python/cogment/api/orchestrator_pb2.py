# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: cogment/api/orchestrator.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from cogment.api import common_pb2 as cogment_dot_api_dot_common__pb2
from cogment.api import agent_pb2 as cogment_dot_api_dot_agent__pb2


DESCRIPTOR = _descriptor.FileDescriptor(
  name='cogment/api/orchestrator.proto',
  package='cogment',
  syntax='proto3',
  serialized_options=None,
  serialized_pb=_b('\n\x1e\x63ogment/api/orchestrator.proto\x12\x07\x63ogment\x1a\x18\x63ogment/api/common.proto\x1a\x17\x63ogment/api/agent.proto\")\n\x15TrialHeartbeatRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\"\x15\n\x13TrialHeartbeatReply\"J\n\x11TrialStartRequest\x12$\n\x06\x63onfig\x18\x01 \x01(\x0b\x32\x14.cogment.TrialConfig\x12\x0f\n\x07user_id\x18\x02 \x01(\t\"6\n\x10TrialJoinRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\x10\n\x08\x61\x63tor_id\x18\x02 \x01(\x05\"\xa2\x01\n\x0fTrialStartReply\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\x10\n\x08\x61\x63tor_id\x18\x02 \x01(\x05\x12*\n\x0chuman_config\x18\x03 \x01(\x0b\x32\x14.cogment.ActorConfig\x12)\n\x0bobservation\x18\x04 \x01(\x0b\x32\x14.cogment.Observation\x12\x14\n\x0c\x61\x63tor_counts\x18\x05 \x03(\x05\"#\n\x0fTrialEndRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\"\x0f\n\rTrialEndReply\"Y\n\x12TrialActionRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\x10\n\x08\x61\x63tor_id\x18\x02 \x01(\x05\x12\x1f\n\x06\x61\x63tion\x18\x03 \x01(\x0b\x32\x0f.cogment.Action\"\xa3\x01\n\x10TrialActionReply\x12)\n\x0bobservation\x18\x01 \x01(\x0b\x32\x14.cogment.Observation\x12\x1f\n\x06reward\x18\x02 \x01(\x0b\x32\x0f.cogment.Reward\x12\x15\n\rtrial_is_over\x18\x03 \x01(\x08\x12,\n\x08messages\x18\x04 \x01(\x0b\x32\x1a.cogment.MessageCollection\"N\n\x14TrialFeedbackRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12$\n\tfeedbacks\x18\x02 \x03(\x0b\x32\x11.cogment.Feedback\"\x14\n\x12TrialFeedbackReply\"K\n\x13TrialMessageRequest\x12\x10\n\x08trial_id\x18\x01 \x01(\t\x12\"\n\x08messages\x18\x02 \x03(\x0b\x32\x10.cogment.Message\"\x13\n\x11TrialMessageReply2\xf6\x04\n\x05Trial\x12?\n\x05Start\x12\x1a.cogment.TrialStartRequest\x1a\x18.cogment.TrialStartReply\"\x00\x12=\n\x04Join\x12\x19.cogment.TrialJoinRequest\x1a\x18.cogment.TrialStartReply\"\x00\x12\x39\n\x03\x45nd\x12\x18.cogment.TrialEndRequest\x1a\x16.cogment.TrialEndReply\"\x00\x12\x42\n\x06\x41\x63tion\x12\x1b.cogment.TrialActionRequest\x1a\x19.cogment.TrialActionReply\"\x00\x12L\n\x0c\x41\x63tionStream\x12\x1b.cogment.TrialActionRequest\x1a\x19.cogment.TrialActionReply\"\x00(\x01\x30\x01\x12K\n\tHeartbeat\x12\x1e.cogment.TrialHeartbeatRequest\x1a\x1c.cogment.TrialHeartbeatReply\"\x00\x12L\n\x0cGiveFeedback\x12\x1d.cogment.TrialFeedbackRequest\x1a\x1b.cogment.TrialFeedbackReply\"\x00\x12:\n\x07Version\x12\x17.cogment.VersionRequest\x1a\x14.cogment.VersionInfo\"\x00\x12I\n\x0bSendMessage\x12\x1c.cogment.TrialMessageRequest\x1a\x1a.cogment.TrialMessageReply\"\x00\x62\x06proto3')
  ,
  dependencies=[cogment_dot_api_dot_common__pb2.DESCRIPTOR,cogment_dot_api_dot_agent__pb2.DESCRIPTOR,])




_TRIALHEARTBEATREQUEST = _descriptor.Descriptor(
  name='TrialHeartbeatRequest',
  full_name='cogment.TrialHeartbeatRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.TrialHeartbeatRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=94,
  serialized_end=135,
)


_TRIALHEARTBEATREPLY = _descriptor.Descriptor(
  name='TrialHeartbeatReply',
  full_name='cogment.TrialHeartbeatReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=137,
  serialized_end=158,
)


_TRIALSTARTREQUEST = _descriptor.Descriptor(
  name='TrialStartRequest',
  full_name='cogment.TrialStartRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='config', full_name='cogment.TrialStartRequest.config', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='user_id', full_name='cogment.TrialStartRequest.user_id', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=160,
  serialized_end=234,
)


_TRIALJOINREQUEST = _descriptor.Descriptor(
  name='TrialJoinRequest',
  full_name='cogment.TrialJoinRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.TrialJoinRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_id', full_name='cogment.TrialJoinRequest.actor_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=236,
  serialized_end=290,
)


_TRIALSTARTREPLY = _descriptor.Descriptor(
  name='TrialStartReply',
  full_name='cogment.TrialStartReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.TrialStartReply.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_id', full_name='cogment.TrialStartReply.actor_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='human_config', full_name='cogment.TrialStartReply.human_config', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='observation', full_name='cogment.TrialStartReply.observation', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_counts', full_name='cogment.TrialStartReply.actor_counts', index=4,
      number=5, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=293,
  serialized_end=455,
)


_TRIALENDREQUEST = _descriptor.Descriptor(
  name='TrialEndRequest',
  full_name='cogment.TrialEndRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.TrialEndRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=457,
  serialized_end=492,
)


_TRIALENDREPLY = _descriptor.Descriptor(
  name='TrialEndReply',
  full_name='cogment.TrialEndReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=494,
  serialized_end=509,
)


_TRIALACTIONREQUEST = _descriptor.Descriptor(
  name='TrialActionRequest',
  full_name='cogment.TrialActionRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.TrialActionRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='actor_id', full_name='cogment.TrialActionRequest.actor_id', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='action', full_name='cogment.TrialActionRequest.action', index=2,
      number=3, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=511,
  serialized_end=600,
)


_TRIALACTIONREPLY = _descriptor.Descriptor(
  name='TrialActionReply',
  full_name='cogment.TrialActionReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='observation', full_name='cogment.TrialActionReply.observation', index=0,
      number=1, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='reward', full_name='cogment.TrialActionReply.reward', index=1,
      number=2, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='trial_is_over', full_name='cogment.TrialActionReply.trial_is_over', index=2,
      number=3, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='messages', full_name='cogment.TrialActionReply.messages', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=603,
  serialized_end=766,
)


_TRIALFEEDBACKREQUEST = _descriptor.Descriptor(
  name='TrialFeedbackRequest',
  full_name='cogment.TrialFeedbackRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.TrialFeedbackRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='feedbacks', full_name='cogment.TrialFeedbackRequest.feedbacks', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=768,
  serialized_end=846,
)


_TRIALFEEDBACKREPLY = _descriptor.Descriptor(
  name='TrialFeedbackReply',
  full_name='cogment.TrialFeedbackReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=848,
  serialized_end=868,
)


_TRIALMESSAGEREQUEST = _descriptor.Descriptor(
  name='TrialMessageRequest',
  full_name='cogment.TrialMessageRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='trial_id', full_name='cogment.TrialMessageRequest.trial_id', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='messages', full_name='cogment.TrialMessageRequest.messages', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=870,
  serialized_end=945,
)


_TRIALMESSAGEREPLY = _descriptor.Descriptor(
  name='TrialMessageReply',
  full_name='cogment.TrialMessageReply',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=947,
  serialized_end=966,
)

_TRIALSTARTREQUEST.fields_by_name['config'].message_type = cogment_dot_api_dot_common__pb2._TRIALCONFIG
_TRIALSTARTREPLY.fields_by_name['human_config'].message_type = cogment_dot_api_dot_common__pb2._ACTORCONFIG
_TRIALSTARTREPLY.fields_by_name['observation'].message_type = cogment_dot_api_dot_common__pb2._OBSERVATION
_TRIALACTIONREQUEST.fields_by_name['action'].message_type = cogment_dot_api_dot_common__pb2._ACTION
_TRIALACTIONREPLY.fields_by_name['observation'].message_type = cogment_dot_api_dot_common__pb2._OBSERVATION
_TRIALACTIONREPLY.fields_by_name['reward'].message_type = cogment_dot_api_dot_agent__pb2._REWARD
_TRIALACTIONREPLY.fields_by_name['messages'].message_type = cogment_dot_api_dot_agent__pb2._MESSAGECOLLECTION
_TRIALFEEDBACKREQUEST.fields_by_name['feedbacks'].message_type = cogment_dot_api_dot_common__pb2._FEEDBACK
_TRIALMESSAGEREQUEST.fields_by_name['messages'].message_type = cogment_dot_api_dot_common__pb2._MESSAGE
DESCRIPTOR.message_types_by_name['TrialHeartbeatRequest'] = _TRIALHEARTBEATREQUEST
DESCRIPTOR.message_types_by_name['TrialHeartbeatReply'] = _TRIALHEARTBEATREPLY
DESCRIPTOR.message_types_by_name['TrialStartRequest'] = _TRIALSTARTREQUEST
DESCRIPTOR.message_types_by_name['TrialJoinRequest'] = _TRIALJOINREQUEST
DESCRIPTOR.message_types_by_name['TrialStartReply'] = _TRIALSTARTREPLY
DESCRIPTOR.message_types_by_name['TrialEndRequest'] = _TRIALENDREQUEST
DESCRIPTOR.message_types_by_name['TrialEndReply'] = _TRIALENDREPLY
DESCRIPTOR.message_types_by_name['TrialActionRequest'] = _TRIALACTIONREQUEST
DESCRIPTOR.message_types_by_name['TrialActionReply'] = _TRIALACTIONREPLY
DESCRIPTOR.message_types_by_name['TrialFeedbackRequest'] = _TRIALFEEDBACKREQUEST
DESCRIPTOR.message_types_by_name['TrialFeedbackReply'] = _TRIALFEEDBACKREPLY
DESCRIPTOR.message_types_by_name['TrialMessageRequest'] = _TRIALMESSAGEREQUEST
DESCRIPTOR.message_types_by_name['TrialMessageReply'] = _TRIALMESSAGEREPLY
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

TrialHeartbeatRequest = _reflection.GeneratedProtocolMessageType('TrialHeartbeatRequest', (_message.Message,), {
  'DESCRIPTOR' : _TRIALHEARTBEATREQUEST,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialHeartbeatRequest)
  })
_sym_db.RegisterMessage(TrialHeartbeatRequest)

TrialHeartbeatReply = _reflection.GeneratedProtocolMessageType('TrialHeartbeatReply', (_message.Message,), {
  'DESCRIPTOR' : _TRIALHEARTBEATREPLY,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialHeartbeatReply)
  })
_sym_db.RegisterMessage(TrialHeartbeatReply)

TrialStartRequest = _reflection.GeneratedProtocolMessageType('TrialStartRequest', (_message.Message,), {
  'DESCRIPTOR' : _TRIALSTARTREQUEST,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialStartRequest)
  })
_sym_db.RegisterMessage(TrialStartRequest)

TrialJoinRequest = _reflection.GeneratedProtocolMessageType('TrialJoinRequest', (_message.Message,), {
  'DESCRIPTOR' : _TRIALJOINREQUEST,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialJoinRequest)
  })
_sym_db.RegisterMessage(TrialJoinRequest)

TrialStartReply = _reflection.GeneratedProtocolMessageType('TrialStartReply', (_message.Message,), {
  'DESCRIPTOR' : _TRIALSTARTREPLY,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialStartReply)
  })
_sym_db.RegisterMessage(TrialStartReply)

TrialEndRequest = _reflection.GeneratedProtocolMessageType('TrialEndRequest', (_message.Message,), {
  'DESCRIPTOR' : _TRIALENDREQUEST,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialEndRequest)
  })
_sym_db.RegisterMessage(TrialEndRequest)

TrialEndReply = _reflection.GeneratedProtocolMessageType('TrialEndReply', (_message.Message,), {
  'DESCRIPTOR' : _TRIALENDREPLY,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialEndReply)
  })
_sym_db.RegisterMessage(TrialEndReply)

TrialActionRequest = _reflection.GeneratedProtocolMessageType('TrialActionRequest', (_message.Message,), {
  'DESCRIPTOR' : _TRIALACTIONREQUEST,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialActionRequest)
  })
_sym_db.RegisterMessage(TrialActionRequest)

TrialActionReply = _reflection.GeneratedProtocolMessageType('TrialActionReply', (_message.Message,), {
  'DESCRIPTOR' : _TRIALACTIONREPLY,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialActionReply)
  })
_sym_db.RegisterMessage(TrialActionReply)

TrialFeedbackRequest = _reflection.GeneratedProtocolMessageType('TrialFeedbackRequest', (_message.Message,), {
  'DESCRIPTOR' : _TRIALFEEDBACKREQUEST,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialFeedbackRequest)
  })
_sym_db.RegisterMessage(TrialFeedbackRequest)

TrialFeedbackReply = _reflection.GeneratedProtocolMessageType('TrialFeedbackReply', (_message.Message,), {
  'DESCRIPTOR' : _TRIALFEEDBACKREPLY,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialFeedbackReply)
  })
_sym_db.RegisterMessage(TrialFeedbackReply)

TrialMessageRequest = _reflection.GeneratedProtocolMessageType('TrialMessageRequest', (_message.Message,), {
  'DESCRIPTOR' : _TRIALMESSAGEREQUEST,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialMessageRequest)
  })
_sym_db.RegisterMessage(TrialMessageRequest)

TrialMessageReply = _reflection.GeneratedProtocolMessageType('TrialMessageReply', (_message.Message,), {
  'DESCRIPTOR' : _TRIALMESSAGEREPLY,
  '__module__' : 'cogment.api.orchestrator_pb2'
  # @@protoc_insertion_point(class_scope:cogment.TrialMessageReply)
  })
_sym_db.RegisterMessage(TrialMessageReply)



_TRIAL = _descriptor.ServiceDescriptor(
  name='Trial',
  full_name='cogment.Trial',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  serialized_start=969,
  serialized_end=1599,
  methods=[
  _descriptor.MethodDescriptor(
    name='Start',
    full_name='cogment.Trial.Start',
    index=0,
    containing_service=None,
    input_type=_TRIALSTARTREQUEST,
    output_type=_TRIALSTARTREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Join',
    full_name='cogment.Trial.Join',
    index=1,
    containing_service=None,
    input_type=_TRIALJOINREQUEST,
    output_type=_TRIALSTARTREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='End',
    full_name='cogment.Trial.End',
    index=2,
    containing_service=None,
    input_type=_TRIALENDREQUEST,
    output_type=_TRIALENDREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Action',
    full_name='cogment.Trial.Action',
    index=3,
    containing_service=None,
    input_type=_TRIALACTIONREQUEST,
    output_type=_TRIALACTIONREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='ActionStream',
    full_name='cogment.Trial.ActionStream',
    index=4,
    containing_service=None,
    input_type=_TRIALACTIONREQUEST,
    output_type=_TRIALACTIONREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Heartbeat',
    full_name='cogment.Trial.Heartbeat',
    index=5,
    containing_service=None,
    input_type=_TRIALHEARTBEATREQUEST,
    output_type=_TRIALHEARTBEATREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='GiveFeedback',
    full_name='cogment.Trial.GiveFeedback',
    index=6,
    containing_service=None,
    input_type=_TRIALFEEDBACKREQUEST,
    output_type=_TRIALFEEDBACKREPLY,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='Version',
    full_name='cogment.Trial.Version',
    index=7,
    containing_service=None,
    input_type=cogment_dot_api_dot_common__pb2._VERSIONREQUEST,
    output_type=cogment_dot_api_dot_common__pb2._VERSIONINFO,
    serialized_options=None,
  ),
  _descriptor.MethodDescriptor(
    name='SendMessage',
    full_name='cogment.Trial.SendMessage',
    index=8,
    containing_service=None,
    input_type=_TRIALMESSAGEREQUEST,
    output_type=_TRIALMESSAGEREPLY,
    serialized_options=None,
  ),
])
_sym_db.RegisterServiceDescriptor(_TRIAL)

DESCRIPTOR.services_by_name['Trial'] = _TRIAL

# @@protoc_insertion_point(module_scope)
