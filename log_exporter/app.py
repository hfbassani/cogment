import cog_settings as settings

import pandas as pd
import time
import os
import base64
# import logging
import grpc
import traceback

from version import VERSION
from distutils.util import strtobool
from grpc_reflection.v1alpha import reflection
from concurrent import futures
from cogment.api.data_pb2 import LogReply, _LOGEXPORTER
from cogment.api.data_pb2_grpc import LogExporterServicer, add_LogExporterServicer_to_server
from cogment.delta_encoding import DecodeObservationData
from google.protobuf.json_format import MessageToJson

from sqlalchemy import create_engine


ENABLE_REFLECTION_VAR_NAME = 'COGMENT_GRPC_REFLECTION'
_ONE_DAY_IN_SECONDS = 60 * 60 * 24
TABLE_NAME = 'test'

engine = create_engine(os.getenv('POSTGRES_ENGINE'), echo=False)


class LogExporterService(LogExporterServicer):
    """Provides methods that implement functionality of log exporter server."""

    def __init__(self):

        self.count_params = 0
        self.count_samples = 0

    @staticmethod
    def convert_value(value, field_descriptor):
        if field_descriptor.type == field_descriptor.TYPE_BYTES:
            value = base64.b64encode(value).decode("utf-8")

        elif field_descriptor.type == field_descriptor.TYPE_MESSAGE:
            value = MessageToJson(value,  including_default_value_fields=True)

        elif field_descriptor.type == field_descriptor.TYPE_ENUM:
            value = field_descriptor.enum_type.values[value].name

        return value

    @staticmethod
    def get_value(obj, field_descriptor):
        value = getattr(obj, field_descriptor.name)

        if field_descriptor.label == field_descriptor.LABEL_REPEATED:
            return [LogExporterService.convert_value(x, field_descriptor) for x in value]

        return LogExporterService.convert_value(value, field_descriptor)

    def Log(self, request_iterator, context):
        try:
            connection = engine.connect()

            for msg in request_iterator:

                if msg.HasField("trial_params"):

                    self.trial_config = MessageToJson(
                        msg.trial_params.trial_config, True)

                    self.COLUMNS = ["trial_id", "tick_id",
                                    "timestamp", "trial_config"]
                    self.last_obs = []

                    self.actor_counts = settings.actor_classes.get_actor_counts(
                        msg.trial_params)

                    for ac_index, actor_class in enumerate(settings.actor_classes):

                        self.count = self.actor_counts[ac_index]

                        cols_obs = [
                            x.name for x in actor_class.observation_space.DESCRIPTOR.fields]
                        cols_actions = [
                            x.name for x in actor_class.action_space.DESCRIPTOR.fields]
                        cols_rewards = ["value", "confidence"]
                        cols_messages = ["message"]
                        self.last_obs.extend([None] * self.count)
                        for j in range(self.count):
                            name = f"{actor_class.id}_{j}"
                            obs_title = [f"{name}_observation"]
                            action_title = [f"{name}_action"]
                            reward_title = [f"{name}_reward_{x}" for x in cols_rewards]
                            message_title = [f"{name}_message_{x}" for x in cols_messages]
                            self.COLUMNS.extend(obs_title)
                            self.COLUMNS.extend(action_title)
                            self.COLUMNS.extend(reward_title)
                            self.COLUMNS.extend(message_title)

                    self.count_params += 1

                elif msg.HasField("sample"):

                    sample = msg.sample

                    row = [
                        sample.trial_id,
                        sample.observations.tick_id,
                        sample.observations.timestamp / 1000000000,
                        self.trial_config
                    ]

                    actor_id = 0
                    for ac_index, actor_class in enumerate(settings.actor_classes):
                        count = self.actor_counts[ac_index]
                        for j in range(count):
                            try:
                                obs_id = sample.observations.actors_map[
                                    actor_id]
                            except Exception:
                                print(sample)

                            obs_data = sample.observations.observations[obs_id]
                            action_data = sample.actions[actor_id].content

                            obs = DecodeObservationData(
                                actor_class, obs_data, self.last_obs[actor_id])
                            self.last_obs[actor_id] = obs

                            row.append(MessageToJson(
                                obs,  including_default_value_fields=True))
                            row.append(MessageToJson(sample.actions[
                                       actor_id],  including_default_value_fields=True))

                            row.append(sample.rewards[actor_id].value)

                            row.append(sample.rewards[actor_id].confidence)

                            row.append(MessageToJson(sample.messages[actor_id], including_default_value_fields=True))

                            actor_id += 1

                    df = pd.DataFrame([row], columns=self.COLUMNS)

                    df.to_sql(TABLE_NAME, connection, if_exists='append')

                    self.count_samples += 1

        except Exception as e:
            print("error:", str(e))
            traceback.print_exc()
            raise
        finally:
            if connection:
                connection.close()

        return LogReply()


def serve():

    server = grpc.server(futures.ThreadPoolExecutor(
        max_workers=1, thread_name_prefix="log_exporter"))
    add_LogExporterServicer_to_server(LogExporterService(), server)

    # Enable grpc reflection if requested
    if strtobool(os.getenv(ENABLE_REFLECTION_VAR_NAME, 'false')):
        SERVICE_NAMES = (_LOGEXPORTER.full_name, reflection.SERVICE_NAME,)

        reflection.enable_server_reflection(SERVICE_NAMES, server)

    server.add_insecure_port('[::]:9000')
    server.start()
    print('server started on port 9000')
    print(f'Log exporter version : {VERSION}')

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':

    # logging.basicConfig()
    serve()
