var services = require('./cogment/api/orchestrator_pb_service.js');
var protos = require('./cogment/api/orchestrator_pb.js');
var env_protos = require('./cogment/api/orchestrator_pb.js');
var common_protos = require('./cogment/api/common_pb.js');
var trial_module = require('./trial.js');
var delta_encoding = require('./delta_encoding.js');
var grpc = require("@improbable-eng/grpc-web").grpc;

class ClientTrial extends trial_module.Trial {
    constructor(conn, trial_start_rep, settings, actor_class, initial_observation, actor_counts, metadata) {
        super(trial_start_rep.getTrialId(), settings, actor_counts);

        this.connection = conn;
        this.actor_class = actor_class;
        this.observation = initial_observation;
        this.actor_id = trial_start_rep.getActorId();
        this.metadata = metadata;
        this.end_calback = null;
        this.messages = null;
        this.onmessage_callback = null;
    }

    set_on_trial_end_event_listener(callback) {
        this.end_calback = callback;
    }

    set_on_message_event_listener(callback) {
        this.onmessage_callback = callback;
    }

    decode_observation(message) {
        this.observation = delta_encoding.decode_observation_data(this.actor_class, message.getObservation().getData(), this.observation);
        this.tick_id = message.getObservation().getTickId();

        if (message.getTrialIsOver()) {
            if (this.end_calback != null) {
                this.end_calback();
            }
        }
    }

    get_messages(message) {
        if (message.getMessages()) {
            this.messages = message.getMessages().getMessagesList();
            if (this.onmessage_callback != null && this.messages != null && this.messages.length != 0) {
                this.messages.forEach(msg => {
                    const deserial = 'proto.' + msg.getPayload().getTypeName() + '.deserializeBinary';
                    const decode = msg.getPayload().unpack(eval(deserial), msg.getPayload().getTypeName());
                    this.onmessage_callback(msg.getSenderId(),decode);
                })
            }
        }
    }

    new_action_request(action) {
        let action_request = new protos.TrialActionRequest();
        action_request.setTrialId(this.id);
        action_request.setActorId(this.actor_id);

        let cogment_action = new common_protos.Action();

        cogment_action.setContent(action.serializeBinary());
        action_request.setAction(cogment_action);

        return action_request;
    }

    async do_action(action) {
        this.flush_feedback();
        this.flush_message();

        let action_request = this.new_action_request(action);

        return new Promise((resolve, reject) => {
            this.connection.client.action(action_request, this.metadata, (err, resp) => {
                if (err) {
                    reject(new Error(`Trial action failure: [${err.code}] ${err.message}`));
                } else {
                    this.decode_observation(resp);
                    this.get_messages(resp);
                    resolve(this.observation);
                }
            });
        });
    }

    start_streaming_action(on_data_available) {
        this.flush_feedback();
        this.flush_message();

        const raw_action_stream = this.connection.client.actionStream();

        raw_action_stream.on('data', message => {
            this.decode_observation(message);
            on_data_available(this.observation);
        });

        return {
            do_action: (action) => {
                raw_action_stream.write(this.new_action_request(action, this.id, this.actor_id));
            }
        }
    }

    end() {
        this.flush_feedback();
        this.flush_message();

        let end_req = new protos.TrialEndRequest();
        end_req.setTrialId(this.id);

        return new Promise((resolve, _) => {
            this.connection.client.end(end_req, this.metadata, (err, resp) => {
                resolve();
            });
        });
    }

    flush_feedback() {
        let feedbacks = this._get_all_feedback();

        if (feedbacks.length > 0) {
            let req = new protos.TrialFeedbackRequest();
            req.setTrialId(this.id);

            req.setFeedbacksList(feedbacks);
            this.connection.client.giveFeedback(req, this.metadata, (_, __) => {
            });
        }
    }

    flush_message() {
        let messages = this._get_all_messages(this.actor_id);

        if (messages.length > 0) {
            let req = new protos.TrialMessageRequest();
            req.setTrialId(this.id);

            req.setMessagesList(messages);
            this.connection.client.sendMessage(req, this.metadata, (_, __) => {
            });
        }
    }

}

class Connection {

    constructor(settings, endpoint, grpcTransport = null) {
        const transport = grpcTransport ? grpcTransport : grpc.CrossBrowserHttpTransport({withCredentials: false});
        this.client = new services.TrialClient(endpoint, {transport: transport});
        this.settings = settings;
    }

    generate_uuid() {
        var dt = new Date().getTime();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }

    async start_trial(actor_class, cfg = undefined) {
        return new Promise((resolve, reject) => {
            var start_req = new protos.TrialStartRequest();
            if (cfg) {
                var cfg_msg = new env_protos.TrialConfig()
                cfg_msg.setContent(cfg.serializeBinary());
                start_req.setConfig(cfg_msg);
            }

            var metadata = {'session_id': this.generate_uuid()};

            this.client.start(start_req, metadata, (err, resp) => {
                if (err) {
                    reject(new Error(`Trial start failure: [${err.code}] ${err.message}`));
                } else {
                    var observation = delta_encoding.decode_observation_data(actor_class, resp.getObservation().getData());

                    var trial = new ClientTrial(this, resp, this.settings, actor_class, observation, resp.getActorCountsList(), metadata);

                    resolve(trial);
                }
            });
        });
    }

    async join_trial(actor_class, trial_id, actor_id) {
        return new Promise((resolve, reject) => {
            var join_req = new protos.TrialJoinRequest();
            join_req.setTrialId(trial_id);
            join_req.setActorId(actor_id);

            const metadata = {}

            this.client.join(join_req, metadata, (err, resp) => {
                if (err) {
                    reject(new Error(`Trial join failure: [${err.code}] ${err.message}`));
                } else {
                    const observation = delta_encoding.decode_observation_data(actor_class, resp.getObservation().getData());

                    const trial = new ClientTrial(this, resp, this.settings, actor_class, observation, resp.getActorCountsList(), metadata);

                    resolve(trial);
                }
            });
        });
    }
}

module.exports.Connection = Connection;