// package: cogment
// file: cogment/api/agent.proto

var cogment_api_agent_pb = require("../../cogment/api/agent_pb");
var cogment_api_common_pb = require("../../cogment/api/common_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var Agent = (function () {
  function Agent() {}
  Agent.serviceName = "cogment.Agent";
  return Agent;
}());

Agent.Start = {
  methodName: "Start",
  service: Agent,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_agent_pb.AgentStartRequest,
  responseType: cogment_api_agent_pb.AgentStartReply
};

Agent.End = {
  methodName: "End",
  service: Agent,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_agent_pb.AgentEndRequest,
  responseType: cogment_api_agent_pb.AgentEndReply
};

Agent.Decide = {
  methodName: "Decide",
  service: Agent,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_agent_pb.AgentDecideRequest,
  responseType: cogment_api_agent_pb.AgentDecideReply
};

Agent.Reward = {
  methodName: "Reward",
  service: Agent,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_agent_pb.AgentRewardRequest,
  responseType: cogment_api_agent_pb.AgentRewardReply
};

Agent.Version = {
  methodName: "Version",
  service: Agent,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_common_pb.VersionRequest,
  responseType: cogment_api_common_pb.VersionInfo
};

Agent.OnMessage = {
  methodName: "OnMessage",
  service: Agent,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_agent_pb.AgentOnMessageRequest,
  responseType: cogment_api_agent_pb.AgentOnMessageReply
};

exports.Agent = Agent;

function AgentClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

AgentClient.prototype.start = function start(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Agent.Start, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AgentClient.prototype.end = function end(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Agent.End, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AgentClient.prototype.decide = function decide(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Agent.Decide, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AgentClient.prototype.reward = function reward(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Agent.Reward, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AgentClient.prototype.version = function version(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Agent.Version, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

AgentClient.prototype.onMessage = function onMessage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Agent.OnMessage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.AgentClient = AgentClient;

