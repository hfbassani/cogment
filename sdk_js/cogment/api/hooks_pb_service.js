// package: cogment
// file: cogment/api/hooks.proto

var cogment_api_hooks_pb = require("../../cogment/api/hooks_pb");
var cogment_api_common_pb = require("../../cogment/api/common_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var TrialHooks = (function () {
  function TrialHooks() {}
  TrialHooks.serviceName = "cogment.TrialHooks";
  return TrialHooks;
}());

TrialHooks.PreTrial = {
  methodName: "PreTrial",
  service: TrialHooks,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_hooks_pb.TrialContext,
  responseType: cogment_api_hooks_pb.TrialContext
};

TrialHooks.Version = {
  methodName: "Version",
  service: TrialHooks,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_common_pb.VersionRequest,
  responseType: cogment_api_common_pb.VersionInfo
};

exports.TrialHooks = TrialHooks;

function TrialHooksClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

TrialHooksClient.prototype.preTrial = function preTrial(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TrialHooks.PreTrial, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialHooksClient.prototype.version = function version(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(TrialHooks.Version, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.TrialHooksClient = TrialHooksClient;

