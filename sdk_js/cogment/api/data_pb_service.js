// package: cogment
// file: cogment/api/data.proto

var cogment_api_data_pb = require("../../cogment/api/data_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var LogExporter = (function () {
  function LogExporter() {}
  LogExporter.serviceName = "cogment.LogExporter";
  return LogExporter;
}());

LogExporter.Log = {
  methodName: "Log",
  service: LogExporter,
  requestStream: true,
  responseStream: false,
  requestType: cogment_api_data_pb.DatalogMsg,
  responseType: cogment_api_data_pb.LogReply
};

exports.LogExporter = LogExporter;

function LogExporterClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

LogExporterClient.prototype.log = function log(metadata) {
  var listeners = {
    end: [],
    status: []
  };
  var client = grpc.client(LogExporter.Log, {
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport
  });
  client.onEnd(function (status, statusMessage, trailers) {
    listeners.status.forEach(function (handler) {
      handler({ code: status, details: statusMessage, metadata: trailers });
    });
    listeners.end.forEach(function (handler) {
      handler({ code: status, details: statusMessage, metadata: trailers });
    });
    listeners = null;
  });
  return {
    on: function (type, handler) {
      listeners[type].push(handler);
      return this;
    },
    write: function (requestMessage) {
      if (!client.started) {
        client.start(metadata);
      }
      client.send(requestMessage);
      return this;
    },
    end: function () {
      client.finishSend();
    },
    cancel: function () {
      listeners = null;
      client.close();
    }
  };
};

exports.LogExporterClient = LogExporterClient;

