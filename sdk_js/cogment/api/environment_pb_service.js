// package: cogment
// file: cogment/api/environment.proto

var cogment_api_environment_pb = require("../../cogment/api/environment_pb");
var cogment_api_common_pb = require("../../cogment/api/common_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var Environment = (function () {
  function Environment() {}
  Environment.serviceName = "cogment.Environment";
  return Environment;
}());

Environment.Start = {
  methodName: "Start",
  service: Environment,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_environment_pb.EnvStartRequest,
  responseType: cogment_api_environment_pb.EnvStartReply
};

Environment.End = {
  methodName: "End",
  service: Environment,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_environment_pb.EnvEndRequest,
  responseType: cogment_api_environment_pb.EnvEndReply
};

Environment.Update = {
  methodName: "Update",
  service: Environment,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_environment_pb.EnvUpdateRequest,
  responseType: cogment_api_environment_pb.EnvUpdateReply
};

Environment.Version = {
  methodName: "Version",
  service: Environment,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_common_pb.VersionRequest,
  responseType: cogment_api_common_pb.VersionInfo
};

Environment.OnMessage = {
  methodName: "OnMessage",
  service: Environment,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_environment_pb.EnvOnMessageRequest,
  responseType: cogment_api_environment_pb.EnvOnMessageReply
};

exports.Environment = Environment;

function EnvironmentClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

EnvironmentClient.prototype.start = function start(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Environment.Start, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

EnvironmentClient.prototype.end = function end(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Environment.End, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

EnvironmentClient.prototype.update = function update(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Environment.Update, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

EnvironmentClient.prototype.version = function version(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Environment.Version, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

EnvironmentClient.prototype.onMessage = function onMessage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Environment.OnMessage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.EnvironmentClient = EnvironmentClient;

