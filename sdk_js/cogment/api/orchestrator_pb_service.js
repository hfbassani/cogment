// package: cogment
// file: cogment/api/orchestrator.proto

var cogment_api_orchestrator_pb = require("../../cogment/api/orchestrator_pb");
var cogment_api_common_pb = require("../../cogment/api/common_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var Trial = (function () {
  function Trial() {}
  Trial.serviceName = "cogment.Trial";
  return Trial;
}());

Trial.Start = {
  methodName: "Start",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_orchestrator_pb.TrialStartRequest,
  responseType: cogment_api_orchestrator_pb.TrialStartReply
};

Trial.Join = {
  methodName: "Join",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_orchestrator_pb.TrialJoinRequest,
  responseType: cogment_api_orchestrator_pb.TrialStartReply
};

Trial.End = {
  methodName: "End",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_orchestrator_pb.TrialEndRequest,
  responseType: cogment_api_orchestrator_pb.TrialEndReply
};

Trial.Action = {
  methodName: "Action",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_orchestrator_pb.TrialActionRequest,
  responseType: cogment_api_orchestrator_pb.TrialActionReply
};

Trial.ActionStream = {
  methodName: "ActionStream",
  service: Trial,
  requestStream: true,
  responseStream: true,
  requestType: cogment_api_orchestrator_pb.TrialActionRequest,
  responseType: cogment_api_orchestrator_pb.TrialActionReply
};

Trial.Heartbeat = {
  methodName: "Heartbeat",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_orchestrator_pb.TrialHeartbeatRequest,
  responseType: cogment_api_orchestrator_pb.TrialHeartbeatReply
};

Trial.GiveFeedback = {
  methodName: "GiveFeedback",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_orchestrator_pb.TrialFeedbackRequest,
  responseType: cogment_api_orchestrator_pb.TrialFeedbackReply
};

Trial.Version = {
  methodName: "Version",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_common_pb.VersionRequest,
  responseType: cogment_api_common_pb.VersionInfo
};

Trial.SendMessage = {
  methodName: "SendMessage",
  service: Trial,
  requestStream: false,
  responseStream: false,
  requestType: cogment_api_orchestrator_pb.TrialMessageRequest,
  responseType: cogment_api_orchestrator_pb.TrialMessageReply
};

exports.Trial = Trial;

function TrialClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

TrialClient.prototype.start = function start(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.Start, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialClient.prototype.join = function join(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.Join, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialClient.prototype.end = function end(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.End, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialClient.prototype.action = function action(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.Action, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialClient.prototype.actionStream = function actionStream(metadata) {
  var listeners = {
    data: [],
    end: [],
    status: []
  };
  var client = grpc.client(Trial.ActionStream, {
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport
  });
  client.onEnd(function (status, statusMessage, trailers) {
    listeners.status.forEach(function (handler) {
      handler({ code: status, details: statusMessage, metadata: trailers });
    });
    listeners.end.forEach(function (handler) {
      handler({ code: status, details: statusMessage, metadata: trailers });
    });
    listeners = null;
  });
  client.onMessage(function (message) {
    listeners.data.forEach(function (handler) {
      handler(message);
    })
  });
  client.start(metadata);
  return {
    on: function (type, handler) {
      listeners[type].push(handler);
      return this;
    },
    write: function (requestMessage) {
      client.send(requestMessage);
      return this;
    },
    end: function () {
      client.finishSend();
    },
    cancel: function () {
      listeners = null;
      client.close();
    }
  };
};

TrialClient.prototype.heartbeat = function heartbeat(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.Heartbeat, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialClient.prototype.giveFeedback = function giveFeedback(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.GiveFeedback, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialClient.prototype.version = function version(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.Version, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

TrialClient.prototype.sendMessage = function sendMessage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(Trial.SendMessage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.TrialClient = TrialClient;

