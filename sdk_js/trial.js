var common_pb = require('./cogment/api/common_pb.js');

class Actor {
  constructor(actor_class, actor_id, trial) {
    this.actor_class = actor_class;
    this.actor_id = actor_id;
    this.feedbacks = [];
    this.messages = [];
    this.trial = trial
  }

  add_feedback(value, confidence, time=undefined) {
    if(time === undefined) {
      time = -1;
    }
    this.feedbacks.push({
      time:time,
      value:value,
      confidence: confidence
    });
  }

  send_message(user_data=undefined, type_name) {
    this.messages.push({
      user_data:user_data,
      type_name:type_name
    });
  }
  
}

class Env {
  constructor(actor_id, trial) {
    this.actor_id = actor_id;
    this.messages = [];
    this.trial = trial
  }

  send_message(user_data, type_name) {
    this.messages.push({
      user_data:user_data,
      type_name:type_name
    });
  }

}

class Trial {
  constructor(id, settings, actor_counts) {
    this.id = id;
    this.settings = settings;
    this.actors = {};
    this.actors.all = [];
    this.tick_id = 0;
    this.actor_counts = actor_counts;
    this.env = {};
    this.env.all = [];

    let actor_id = 0;

    let a_c_names = Object.keys(settings.actor_classes);

    for(var i in actor_counts) {
      let actor_list = [];
      let a_c = settings.actor_classes[a_c_names[i]];
      let count = actor_counts[i];

      for(var j = 0 ; j < count; ++j) {
        let act = new Actor(a_c, actor_id, this);
        actor_list.push(act);
        this.actors.all.push(act)
        ++actor_id;
      }
      
      this.actors[a_c_names[i]] = actor_list;
    }

    let env_list = [];
    let env = new Env(-1, this);
    env_list.push(env);
    this.env.all.push(env);
    let env_class = settings.env_class;
    this.env['env'] = env_list;

  }

  _get_all_feedback() {
    let result = []
 
    for(const actor of this.actors.all) {
      for(const src_fb of actor.feedbacks) {
        let fb = new common_pb.Feedback();
        fb.setActorId(actor.actor_id);
        fb.setTickId(src_fb.time);
        fb.setValue(src_fb.value);
        fb.setConfidence(src_fb.confidence);

        result.push(fb);
      }

      actor.feedbacks = [];
    }

    return result;
  }

  _get_all_messages(source_id) {

    let result = []
    for(const actor of this.actors.all) {
      for(const src_msg of actor.messages) {
        let msg = new common_pb.Message();
        msg.setSenderId(source_id);
        msg.setReceiverId(actor.actor_id);

        if (src_msg) {
          const tst = src_msg.user_data.serializeBinary();
          const any = new proto.Any();
          any.pack(tst,src_msg.type_name);
          msg.setPayload(any);
        }
        result.push(msg);
      }

      actor.messages = [];
    }

    for(const env of this.env.all) {
      for(const src_msg of env.messages) {
        let msg = new common_pb.Message();
        msg.setSenderId(source_id);
        msg.setReceiverId(env.actor_id);

        if (src_msg) {
          const tst = src_msg.user_data.serializeBinary();
          const any = new proto.Any();
          any.pack(tst,src_msg.type_name);
          msg.setPayload(any);
        }
        result.push(msg);
      }

      env.messages = [];
    }

    return result;

  }

  multi_cast(user_data, type_name, send_list=null) {

    if (send_list != null) {
      let possible_targets = Array.from(Array(this.actor_counts.reduce((a, b) => a + b, 0)).keys());
      possible_targets.push(-1);

      for(const target of send_list) {
        if (possible_targets.includes(target)) {
          if (target == -1) {
            this.env.env[0].send_message(user_data=user_data, type_name=type_name);
          }
          else if (target == 0) {
            this.actors.human[0].send_message(user_data=user_data, type_name=type_name);
          }
          else {
            this.actors.agent[target-1].send_message(user_data=user_data, type_name=type_name);
          }

        } 
      }
    }
  }


}

module.exports.Trial = Trial;
