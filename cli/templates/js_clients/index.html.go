package js_clients

const INDEX_HTML = `
<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{.ProjectName}}</title>
    <script src="./main.js"></script>
</head>
<body>
<h1>Welcome to your {{.ProjectName}} project!</h1>
<h3>Right click - Inspect - Console</h3>
</body>
</html>
`
