package templates

const ROOT_GITIGNORE = `
__pycache__/
docker-compose.override.yaml
data_pb2.py
data_pb2.pyi
cog_settings.py
clients/js/cog_settings.js
clients/js/node_modules

`
